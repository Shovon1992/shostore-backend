let master = require("./master");
var excute = require("../mariaDbCon/exe");

module.exports = {
  /** get all resturant list  */
  createMasterColor: function (data, callback) {
    var sql = master.master;
    console.log({
      "create master task": "create_master_color",
      data: JSON.stringify(data),
    });
    excute.execute(sql, { task: "create_master_color", data: data }, callback);
  },
  /** get all resturant list  */
  getMasterColor: function (callback) {
    var sql = master.master;
    //console.log({'task': 'get_master_color','data': JSON.stringify(data)});
    excute.execute(sql, { task: "get_master_color", data: "{}" }, callback);
  },

  getDashboard: function (callback) {
    var sql = master.master;
    excute.execute(sql, { task: "dashboard_list", data: "{}" }, callback);
  },

  /** Subscription plans  */

  getPlanList: function (callback) {
    var sql = master.master;
    excute.execute(sql, { task: "get_subcription_plan", data: "{}" }, callback);
  },
  createPlan: function (data, callback) {
    var sql = master.master;
    excute.execute(sql, { task: "create_master_plan", data: data }, callback);
  },

  updatePlan: function (data, callback) {
    var sql = master.master;
    excute.execute(
      sql,
      { task: "update_subscription_plan", data: data },
      callback
    );
  },
    deletePlan: function (data, callback) {
      console.log("delete", data);
    var sql = master.master;
    excute.execute(sql, { task: "delete_plan", data: data }, callback);
  },
};
