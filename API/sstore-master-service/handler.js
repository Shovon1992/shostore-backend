"use strict";
const dbService = require("./database/store/masterDbService");
module.exports.msaterColor = (event, context, callback) => {
  switch (event.httpMethod) {
    case "GET":
      dbService.getMasterColor(callback);
      break;
    case "POST":
      dbService.createMasterColor(event.body, callback);
      break;
    default:
      dbService.getMasterColor(callback);
      break;
  }

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};

module.exports.util = (event, context, callback) => {
  switch (event.httpMethod) {
    case "GET":
      let pathParameter = event.pathParameters;
      console.log("path---", event.pathParameters);
      if (pathParameter.type == "dashboard") {
        dbService.getDashboard(callback);
      }
      break;
  }
};

/** subscription pla creTION  */
module.exports.subscription = (event, contxt, callback) => {
  switch (event.httpMethod) {
    case "GET":
      dbService.getPlanList(callback);
      break;
    case "POST":
      
      dbService.createPlan(event.body, callback);
      break;
    case "PUT":
      dbService.updatePlan(event.body, callback);
      break;
    case "DELETE":

      dbService.deletePlan(event.body, callback);
      break;
  }
};
