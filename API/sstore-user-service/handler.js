'use strict';
const dbService = require('./database/store/userDbService');


/** For creating user and  geting personal details*/
module.exports.user = (event, context, callback) => {
  switch(event.httpMethod) {
    case 'GET' : 
      dbService.getUser(event.pathParameters, callback)
      break;
    case 'POST' :
      const data = {
        'authId': event.userName, 
        'phone': event.request.userAttributes.phone_number,
        'email':event.request.userAttributes.email,
        'name':event.request.userAttributes.name,
        'gender':event.request.userAttributes.name
      };
      dbService.createUser(data, (res,err)=>{
        context.succeed(event);
      });
      break;

  }
}

module.exports.createUser = (event, context, callback) => {
  
      const data = {
        'authId': event.userName, 
        'phone': event.request.userAttributes.phone_number,
        'email':event.request.userAttributes.email,
        'name':event.request.userAttributes.name,
        'gender':event.request.userAttributes.gender
      };
      dbService.createUser(data, (res,err)=>{
        context.succeed(event);
      });
}


/**
 * User wish lists 
 */

 module.exports.wishlist = (event, context, callback) => {
  switch(event.httpMethod) {
    case 'GET' : 
      dbService.getWishlist(event.pathParameters,callback);
      break;
    case 'POST' :
      dbService.createWishlist(event.body,callback);
      break;
    case 'DELETE' :
      dbService.removeFromWishlist(event.pathParameters,callback);
      break;


  }
}


/** 
 * product Ratings
 */

module.exports.producRating = (event,context,callback) =>{
  switch(event.httpMethod) {
    case 'GET' : 
      dbService.getProductRating(event.pathParameters, callback);
      break;
    case 'POST' :
      dbService.addProductRating(event.body, callback);
      break;
  }
}


/** 
 * seller Ratings
 */

 module.exports.sellerRating = (event,context,callback) =>{
  switch(event.httpMethod) {
    case 'POST' :
      dbService.addSellerRatings(event.body, callback);
      break;
  }
}



// module.exports.producRating = (event,context,callback) =>{
//   // switch(event.httpMethod) {
//   //   case 'GET' : 
//       //dbService.getProductRating(event.pathParameters, callback);
//      // break;
//     // case 'POST' :
//     dbService.addProductRating(event.body, callback);
//     //   break;
//   //}
// }