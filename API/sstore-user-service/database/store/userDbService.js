let user = require('./user');
var excute = require('../mariaDbCon/exe');
var sql = user.user;

module.exports = {

    /** create new seller  */
    createUser: function(data, callback){
      console.log({'task': 'create_user','data': JSON.stringify(data)});
      excute.execute(sql, {'task': 'create_user','data': JSON.stringify(data)}, callback);
    },

    /** Get user details by id */
    
    getUser: function(data, callback){
      console.log({'task': 'create_user','data': JSON.stringify(data)});
      excute.execute(sql, {'task': 'get_user_details','data': JSON.stringify(data)}, callback);
    },

    /** Create user wishlist */
    createWishlist: function(data, callback){
      console.log({'task': 'create_user','data': JSON.stringify(data)});
      excute.execute(sql, {'task': 'create_wishlist','data': (data)}, callback);
    },

    /** Fetch user wishlist */
    getWishlist: function(data, callback){
      console.log({'task': 'create_user','data': JSON.stringify(data)});
      excute.execute(sql, {'task': 'get_user_wishlist','data': JSON.stringify(data)}, callback);
    },

    removeFromWishlist: function(data, callback){
      console.log({'task': 'create_user','data': JSON.stringify(data)});
      excute.execute(sql, {'task': 'remove_from_wishlist','data': JSON.stringify(data)}, callback);
    },
    /**
     * insert product rating
     */
    addProductRating : function(data, callback) {
      
      data = JSON.parse(data)
      console.log({'task': data.user_id,'data of adding product': typeof(data)});
      insertData = {
        user_id : data.user_id,
        rating : data.rating,
        comment : data.comment,
        rating_for : 'Product',
        shop_id : data.shop_id,
        product_id: data.product_id
      } 
      console.log(insertData)
    excute.execute(sql, {'task': 'add_rating','data': JSON.stringify(insertData) }, callback);
    },
    
    /**
     * insert product rating
     */
     getProductRating : function(data, callback) {
      excute.execute(sql, {'task': 'get_product_rating','data': JSON.stringify(data)}, callback);
    },

    /** Adding seller ratings  */
    addSellerRatings : function(data, callback) {
      
      data = JSON.parse(data)
      // console.log({'task': data.user_id,'data of adding product': typeof(data)});
      insertData = {
        user_id : data.user_id,
        rating : data.rating,
        comment : data.comment,
        rating_for : 'Seller',
        shop_id : data.shop_id,
      } 
      // console.log(insertData)
    excute.execute(sql, {'task': 'add_seller_rating','data': JSON.stringify(insertData) }, callback);
    },

    /** End of class */
}