const {createPool}      = require('mysql');

exports.establishConnection = function() {
  const pool = createPool({
    host     : 'shoestore-dev-db.cvpjmjddx6gh.us-east-1.rds.amazonaws.com',
    user     : 'admin',
    password : '#Prady654',
    database : 'shoestore_dev_db',
    connectionLimit:10,
    multipleStatements:true,

  });

  pool.config.connectionConfig.queryFormat = function (query, values) {
    if (!values) return query;
    return query.replace(/\:(\w+)/g, function (txt, key) {
      if (values.hasOwnProperty(key)) {
        return this.escape(values[key]);
      }
      return txt;
    }.bind(this));
  };

  return pool;
}