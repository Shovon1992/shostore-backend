"use strict";
/**Declare db services */
const dbService = require("./database/store/superAdminDbService");

module.exports.hello = (event) => {
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: "Go Serverless v1.0! Your function executed successfully!",
        input: event,
      },
      null,
      2
    ),
  };

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};

/** Super admin create abd get details */
module.exports.superAdmin = (event, context, callback) => {
  const data = {
    authId: event.userName,
    phone: event.request.userAttributes.phone_number,
    email: event.request.userAttributes.email,
    name: event.request.userAttributes.name,
  };
  dbService.createSuperAdmin(data, (res, err) => {
    console.log("return-----", res, err);
    context.succeed(event);
  });
};

/**
 * Get get admin details and update
 */

module.exports.admin = (event, context, callback) => {
  switch (event.httpMethod) {
    case "GET":
      let pathParameter = event.pathParameters;
      console.log(event);
      //if(pathParameter){
      dbService.getAdminDetails(pathParameter, callback);
      //}

      break;
    default:
      const pathParameter1 = event.pathParameters;
      console.log("event from handler", event);
      //if(pathParameter){
      dbService.getAdminDetails(event.body, callback);
      //}
      break;
  }
};

/** Add subscription plan  */
module.exports.subscription = (event, context, callback) => {
  switch (event.httpMethod) {
    case "GET":
      dbService.getSellerSubscriptionList(callback);
      break;
    case "POST":
      dbService.addSellerSubscription(event.body, callback);
      break;
    case "PUT":
      dbService.updateSellerSubscription(event.body, callback);
      break;
  }
};
 