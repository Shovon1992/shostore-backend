let superAdmin = require("./superAdmin");
var excute = require("../mariaDbCon/exe");
var sql = superAdmin.superAdmin;

module.exports = {
  /** get all resturant list  */
  createSuperAdmin: function (data, callback) {
    var sql = superAdmin.superAdmin;
    console.log({ task: "create_admin", data: JSON.stringify(data) });
    excute.execute(
      sql,
      { task: "create_admin", data: JSON.stringify(data) },
      callback
    );
  },
  /** get admin detalis  */
  getAdminDetails: function (data, callback) {
    var sql = superAdmin.superAdmin;
    tempdata = {
      authId: data.userId,
    };
    console.log("form db service", {
      task: "get_admin_details",
      data: JSON.stringify(tempdata),
    });
    excute.execute(
      sql,
      { task: "get_admin_details", data: JSON.stringify(tempdata) },
      callback
    );
  },
  /** get admin detalis  */
  getSellerSubscriptionList: function (callback) {
    excute.execute(
      sql,
      { task: "subscription_list_by_type", data: "{}" },
      callback
    );
  },

  /** get admin detalis  */
    updateSellerSubscription: function (data, callback) {
      data.subscription_date = new Date(data.subscription_date);
    excute.execute(
      sql,
      { task: "update_seller_subscription", data: data },
      callback
    );
  },
  /** get admin detalis  */
  addSellerSubscription: function (data, callback) {
    data.subscription_date = new Date(data.subscription_date);
    excute.execute(
      sql,
      { task: "add_seller_subscription", data: data },
      callback
    );
  },
};
