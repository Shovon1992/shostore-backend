"use strict";
const dbservice = require("./database/store/productDbService");

module.exports.hello = (event) => {
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: "Go Serverless v1.0! Your function executed successfully!",
        input: event,
      },
      null,
      2
    ),
  };

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};

/** Admin product listing and manage*/

module.exports.adminProduct = (event, context, callback) => {
  switch (event.httpMethod) {
    case "GET":
      let pathParameter = event.pathParameters;
      if (!pathParameter) {
        /* if no path paramater exist then show all product created by admin*/
        dbservice.getAllProduct(callback);
      } else {
        console.log(pathParameter);
        dbservice.getAllProductNotInShop(event.pathParameters, callback);
      }
      break;
    case "POST":
      console.log("Create new Product ", event);
      dbservice.createNewProduct(event.body, callback);
    default:
      dbservice.getAllProduct(callback);
      break;
  }
};

/** Admin product size update  */

module.exports.adminProductSize = (event, context, callback) => {
  switch (event.httpMethod) {
    case "POST":
      console.log(
        "--------------------------dddd",
        event.body,
        event.body.sizes
      );
      dbservice.createNewProductSize(event.body, callback);
      break;
    default:
      dbservice.createNewProductSize(event.body, callback);
      break;
  }
};

/** Admin product size update  */

module.exports.adminProductColor = (event, context, callback) => {
  switch (event.httpMethod) {
    case "POST":
      dbservice.createNewProductColor(event.body, callback);
      break;
  }
};

module.exports.productImages = (event, context, callback) => {
  switch (event.httpMethod) {
    case "POST":
      dbservice.insertProductImages(event.body, callback);
      break;
  }
};

module.exports.ratings = (event, context, callback) => {
  switch (event.httpMethod) {
    case "GET":
      console.log(event.pathParameters);
      dbservice.getRatings(event.pathParameters, callback);
      break;
  }
};

module.exports.sellerProduct = (event, context, callback) => {
  switch (event.httpMethod) {
    case "POST":
      console.log(event.pathParameters);
      dbservice.shopProductDetailsByColorSize(event.body, callback);
      break;
  }
};

/** Get the product color , size list of a perticular  shop product */

module.exports.colorPriceInfoOfProduct = (event, contxt, callback) => {
  switch (event.httpMethod) {
    case "GET":
      console.log(event.pathParameters);
      dbservice.shopProductCOlorSizeDetailsByMasterProductShop(
        event.pathParameters,
        callback
      );
      break;
  }
};
