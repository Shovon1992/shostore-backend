let product = require("./product");
var excute = require("../mariaDbCon/exe");

module.exports = {
  /** get all product list created by admin */
  getAllProduct: function (callback) {
    var sql = product.product;
    excute.execute(sql, { task: "get_all_product", data: "{}" }, callback);
  },

  /** get all product list created by admin */
  getAllProductNotInShop: function (data, callback) {
    console.log("hghjggjhgjgj", data);
    var sql = product.product;
    excute.execute(
      sql,
      { task: "get_all_product_not_in_shop", data: JSON.stringify(data) },
      callback
    );
  },

  /** Create new product creted by admin */
  createNewProduct: function (data, callback) {
    var sql = product.product;
    console.log("product details", JSON.stringify(data));
    excute.execute(sql, { task: "create_new_product", data: data }, callback);
  },

  /** Create new product creted by admin */
  createNewProductSize: function (data, callback) {
    var sql = product.product;
    console.log(data, data["producti_id"]);
    data = JSON.parse(data);

    data["sizes"].forEach((element) => {
      console.log(element);
      let tempData = {
        producti_id: data.producti_id,
        size: element.size,
        length: element.length,
      };
      console.log(tempData);
      excute.execute(
        sql,
        { task: "add_product_size", data: JSON.stringify(tempData) },
        callback
      );
    });
  },

  /** Create new product creted by admin */
  createNewProductColor: function (data, callback) {
    var sql = product.product;
    console.log(data, data["producti_id"]);
    data = JSON.parse(data);
    data["colors"].forEach((element) => {
      console.log(element);
      let tempData = {
        producti_id: data.producti_id,
        color_id: element.color_id,
      };
      console.log(tempData);
      excute.execute(
        sql,
        { task: "add_product_color", data: JSON.stringify(tempData) },
        callback
      );
    });
  },

  insertProductImages: function (data, callback) {
    var sql = product.product;
    data = JSON.parse(data);
    data["images"].forEach((element) => {
      console.log(element);
      let tempData = {
        product_color_id: data.product_color_id,
        product_image: element.product_image,
        product_image_thumb: element.product_image_thumb,
      };
      console.log(tempData);
      excute.execute(
        sql,
        { task: "add_product_images", data: JSON.stringify(tempData) },
        callback
      );
    });
  },

  /** get product ratings */
  getRatings: function (data, callback) {
    // data = JSON.parse(data)
    console.log(data);
    task =
      data.rating_for == "product"
        ? "get_product_rating"
        : "get_product_rating";
    tempData = {
      id: data.id,
    };

    var sql = product.product;
    excute.execute(
      sql,
      { task: task, data: JSON.stringify(tempData) },
      callback
    );
  },
  shopProductDetailsByColorSize: function (data, callback) {
    var sql = product.product;
    console.log("product details", JSON.stringify(data));
    excute.execute(
      sql,
      { task: "product_details_by_color_size", data: data },
      callback
    );
  },

  shopProductCOlorSizeDetailsByMasterProductShop: function (data, callback) {
    var sql = product.product;
    console.log("product details", JSON.stringify(data));
    excute.execute(
      sql,
      { task: "color_size_list_shop_product", data: JSON.stringify(data) },
      callback
    );
  },
};
