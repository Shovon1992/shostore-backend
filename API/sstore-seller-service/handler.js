'use strict';
const dbService = require('./database/store/sellerDbService')
// module.exports.hello = async event => {
//   return {
//     statusCode: 200,
//     body: JSON.stringify(
//       {
//         message: 'Go Serverless v1.0! Your function executed successfully!',
//         input: event,
//       },
//       null,
//       2
//     ),
//   };

//   // Use this code if you don't use the http event with the LAMBDA-PROXY integration
//   // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
// };


/** Create seller */

module.exports.createSeller = (event, context, callback) => {
  console.log('event ------',event)
  const data = {
    'authId': event.userName, 
    'phone': event.request.userAttributes.phone_number,
    'email':event.request.userAttributes.email,
    'name':event.request.userAttributes.name
  };
  dbService.createSeller(data, (res,err)=>{
    console.log('return-----',res,err)
    context.succeed(event);
  });
}

/**Seller list and all the things */

module.exports.seller = (event, context, callback) => {
  
  switch(event.httpMethod) {
    case 'GET' :
      if(event.pathParameters){
        dbService.getSellerById(event.pathParameters,callback);
      } else {
        dbService.getAllSeller(callback);
      }
      
      break;
    case 'PUT' :
      dbService.updateSellerStatus(event.body, callback);
      break;
    default:
      break;  
  }
}


module.exports.citySeller = (event, context, callback) => {
  
  switch(event.httpMethod) {
    case 'GET' :
      if(event.pathParameters){
        dbService.getSellerByLocation(event.pathParameters,callback);
      }else{
        dbService.getCityList(callback);
      }
      break;
    default:
      break;  
  }
}
/** Shop address */
module.exports.shopAddress = (event, context, callback) => {
  
  switch (event.httpMethod) {
    case "POST":
      dbService.addSellerAddress(event.body, callback);
      break;
    case "PUT":
      dbService.updateSellerAddress(event.body, callback);
      break;
    default:
      break;
  }
}

/** Shop Images add */
module.exports.shopImage = (event, context, callback) => {
  
  switch(event.httpMethod) {
    case 'POST' :
      dbService.addShopImage(event.body, callback);
      break;
    default:
      break;  
  }
}
/** Shop Owner details add */
module.exports.addShopOwner = (event, context, callback) => {
  
  switch (event.httpMethod) {
    case "POST":
      dbService.addShopOwner(event.body, callback);
      break;
    case "PUT":
      dbService.addShopOwner(event.body, callback);
      break;
    default:
      break;
  }
}

/** Shop Images add */
module.exports.addShopOwnerAddress = (event, context, callback) => {
  
  switch(event.httpMethod) {
    case 'POST' :
      dbService.addShopOwnerAddress(event.body, callback);
      break;
    default:
      break;  
  }
}

/** Shop Verification  add */
module.exports.shopVerification = (event, context, callback) => {
  
  switch(event.httpMethod) {
    case 'POST' :
      dbService.addShopVerification(event.body, callback);
      break;
    default:
      break;  
  }
}


/** Shop owner Verification  add */
module.exports.shopOwnerVerification = (event, context, callback) => {
  
  switch(event.httpMethod) {
    case 'POST' :
      dbService.addShopOwnerVerification(event.body, callback);
      break;
    default:
      break;  
  }
}


/** Shop product  */

module.exports.shopProduct = (event, context, callback) => {
  
  switch(event.httpMethod) {
    case 'POST' :
      dbService.addShopProduct(event.body, callback);
      break;
    case 'GET' :
      dbService.shopProductList(event.pathParameters, callback)
      break;
    default:
      dbService.shopProductList(event.pathParameters, callback)
      break;  
  }
}

/** Shop product details  */

module.exports.shopProductDetails =(event, context, callback) => {
  dbService.shopProductDetails(event.pathParameters, callback);
}

/** Get all the city list who are selling the same product */
module.exports.otherSellerLists = (event, context, callback) => {
  dbService.getOtherSellerLists(event.body, callback);
}
/**shop city lists  */
module.exports.shopCity =(event, context, callback) => {
  dbService.getCityList(callback);
}

/**Seller subscription list of aparticular seller
 * 
 */

module.exports.subscription = (event, context, callback) => {
  switch (event.httpMethod) {
    case 'GET':
      dbService.getSellerSubscriptionList(event.pathParameters, callback);
  }
}
/** seller shop address add */