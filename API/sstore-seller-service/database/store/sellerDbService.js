let seller = require("./seller");
var excute = require("../mariaDbCon/exe");
var sql = seller.seller;

module.exports = {
  /** create new seller  */
  createSeller: function (data, callback) {
    console.log({ task: "create_admin", data: JSON.stringify(data) });
    excute.execute(
      sql,
      { task: "create_seller", data: JSON.stringify(data) },
      callback
    );
  },

  /** Get all seller list */
  getAllSeller: function (callback) {
    excute.execute(sql, { task: "seller_list", data: "{}" }, callback);
  },
  /**GET seller list by loactionlocation  */
  getSellerByLocation: function (data, callback) {
    excute.execute(
      sql,
      { task: "seller_list_city", data: JSON.stringify(data) },
      callback
    );
  },
  /** Get all seller list */
  getSellerById: function (data, callback) {
    console.log("kdjhfkdshkfhdkfhkdsh----------", data);
    excute.execute(
      sql,
      { task: "seller_details_by_auth", data: JSON.stringify(data) },
      callback
    );
  },
  /**Update seller stauts */
  updateSellerStatus: function (data, callback) {
    excute.execute(sql, { task: "change_seller_status", data: data }, callback);
  },

  /** Add shop address  */
  addSellerAddress: function (data, callback) {
    excute.execute(sql, { task: "add_shop_address", data: data }, callback);
  },

  /** update shop address  */
  updateSellerAddress: function (data, callback) {
    excute.execute(sql, { task: "update_shop_address", data: data }, callback);
  },

  /** Add shop address  */
  addShopImage: function (data, callback) {
    excute.execute(sql, { task: "add_shop_image", data: data }, callback);
  },
  /** Add Owner information  */
  addShopOwner: function (data, callback) {
    excute.execute(sql, { task: "add_shop_owner", data: data }, callback);
  },

  /** update  Owner information  */
  addShopOwner: function (data, callback) {
    excute.execute(sql, { task: "update_shop_owner", data: data }, callback);
  },

  /** Add shop address  */
  addShopOwnerAddress: function (data, callback) {
    excute.execute(
      sql,
      { task: "add_shop_owner_address", data: data },
      callback
    );
  },

  /** Add shop verification  */
  addShopVerification: function (data, callback) {
    excute.execute(sql, { task: "shop_verification", data: data }, callback);
  },

  /** Add shop owner verification  */
  addShopOwnerVerification: function (data, callback) {
    excute.execute(
      sql,
      { task: "shop_verification_owner", data: data },
      callback
    );
  },

  /** Add shop product  */
  addShopProduct: function (data, callback) {
    excute.execute(sql, { task: "add_shop_product", data: data }, callback);
  },
  /** Add shop product  */
  addShopProduct: function (data, callback) {
    excute.execute(sql, { task: "add_shop_product", data: data }, callback);
  },
  /** Add shop product  */
  shopProductList: function (data, callback) {
    console.log(data);
    excute.execute(
      sql,
      { task: "all_shop_product", data: JSON.stringify(data) },
      callback
    );
  },
  /** Add shop product  */
  shopProductDetails: function (data, callback) {
    excute.execute(
      sql,
      { task: "shop_product_details", data: JSON.stringify(data) },
      callback
    );
  },

  /** Get the seller list selling the same product  */
  getOtherSellerLists: function (data, callback) {
    console.log("city data ", data, JSON.stringify(data));
    excute.execute(sql, { task: "other_seller_list", data: data }, callback);
  },

  /**get city list  */
  getCityList: function (callback) {
    excute.execute(sql, { task: "shop_cities", data: "{}" }, callback);
  },

  /** Get the subscription list of particular seller */
  getSellerSubscriptionList: function (data, callback) {
    console.log('----',JSON.stringify(data));
    excute.execute(
      sql,
      { task: "get_seller_subscription_list", data: JSON.stringify(data) },
      callback
    );
  },
};
