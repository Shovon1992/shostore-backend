CREATE DEFINER
=`admin`@`%` PROCEDURE `super_admin_procedure`
(in task text
(20), in input_data json)
BEGIN
    declare ihash_id text
    (255) default null;
case task
    
		# For creating new super admin
		when 'create_admin' then
set @name = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.name') );
set @email = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.email') );
set @phone = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.phone') );
set @status = 1;
set @uid = (SELECT AUTO_INCREMENT
FROM information_schema.TABLES
WHERE TABLE_NAME = "super_admin");
set @hash_id = (SELECT SHA1(CONCAT(now(),@uId,'super_admin')));

set @auth_id = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.authId') );
insert into super_admin
    (
    name,
    email,
    phone,
    status,
    hash_id,
    auth_id
    )
values(
        @name,
        @email,
        @phone,
        1,
        createHashId("super_admin"),
        @auth_id
            );


#get get admin details
        when 'get_admin_details' then
set ihash_id
= JSON_UNQUOTE
( JSON_EXTRACT
(input_data,'$.authId') );
select hash_id as userId, name, email, phone, status
from super_admin
where auth_id = ihash_id and status = 1 and email is not null;



#
Add Seller subscription 
    when 'add_seller_subscription' then
set @amount = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.amount') );
set @subscription_date = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.subscription_date') );
set @plan_id = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.plan_id') );
set subscription_plan_id
=
(select id
from subscription_plan
where hash_id = @plan_id );
set @subscriptionCode = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.subscriptionCode') );
set @payment_mode = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.payment_mode') );
set @ref_id = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.ref_id') );
set @shop_id = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.shop_id') );

insert into seller_subscription
    (
    amount,
    subscription_date,
    subscription_plan_id,
    subscriptionCode,
    payment_mode,
    ref_id,
    shop_id,
    status,
    hash_id
    )
values
    (
        amount,
        subscription_date,
        subscription_plan_id,
        subscriptionCode,
        payment_mode,
        ref_id,
        shop_id,
        1,
        createHashId("shop_verification")
    );

#
update Seller subscription 
    when 'update_seller_subscription' then
set @seller_subscription_id = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.seller_subscription_id') );
set @amount = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.amount') );
set @subscription_date = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.subscription_date') );
set @plan_id = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.plan_id') );
set subscription_plan_id
=
(select id
from subscription_plan
where hash_id = @plan_id );
set @subscriptionCode = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.subscriptionCode') );
set @payment_mode = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.payment_mode') );
set @ref_id = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.ref_id') );
set @shop_id = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.shop_id') );

update  seller_subscription
		set 
			amount = @amount,
			subscription_date = @subscription_date,
			subscription_plan_id = @subscription_plan_id,
			subscriptionCode = @subscriptionCode,
			payment_mode = @payment_mode,
			ref_id = @ref_id,
			shop_id = @shop_id
		where id  > 1 and hash_id = @seller_subscription_id;
when 'subscription_list_by_type' then
select
    shop.name as shop_name,
    sSub.hash_id as seller_subscription_id,
    sSub.amount as subscription_amount,
    sSub.subscription_date as subscription_date,
    sSub.activation_status as activation_status,
    sSub.payment_mode as payment_mode,
    sSub.ref_id as ref_id,
    sSub.subscriptionCode as subscription_code,
    sPlan.plan_name
as plan,
            sPlan.duration
            
        from seller_subscription sSub
        inner join seller shop on shop.id = sSub.shop_id
        inner join subscription_plan sPlan on sPlan.id = sSub.subscription_plan_id
        ;
end case;
END