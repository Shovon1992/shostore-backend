CREATE DEFINER=`admin`@`%` PROCEDURE `master_procedure`(in task text(20), in input_data json)
BEGIN
	declare ihash_id text(255) default null;
    case task
    
    # For creating master colors
		when 'create_master_color' then
			set @name = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.name') );
			set @color_image = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.color_image') );
            set @color_image_thumb = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.color_image_thumb') );
            
            insert into all_product_color(
				name,
				color_image,
				color_image_thumb,
				hash_id
            ) values (
				@name,
				@color_image,
				@color_image_thumb,
                createHashId("all_product_color")
            );
            
		when 'get_master_color' then
			select * from all_product_color;
            
		when 'create_master_subscription_plan' then
			set @plan_name = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.plan_name') );
			set @amount = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.amount') );
            set @duration = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.duration') );
            
            insert into subscription_plan(
				plan_name,
				amount,
				duration,
				hash_id
            ) values (
				@plan_name,
				@amount,
				@duration,
                createHashId("subscription_plan")
            );
		when 'dashboard_list' then 
			select 
				(select count(*) from seller where status = 1) as total_seller,
				(select count(*) from product where status = 1) as total_product,
				(select count(*) from product user where status = 1) as total_user,
				(select count(distinct city) from shop_address where status = 1) as total_cities;
		# For creating master colors
		when 'create_master_plan' then
			set @plan_name = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.plan_name') );
			set @amount = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.amount') );
            set @duration = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.duration') );
            -- select @plan_name,@amount,@duration; 
            insert into subscription_plan(
				plan_name,
				amount,
				duration,
                status,
				hash_id
            ) values (
				@plan_name,
				@amount,
				@duration,
                1,
                createHashId("subscription_plan")
            );
		when 'get_subcription_plan' then 
			select hash_id as plan_id, plan_name, amount, duration from subscription_plan where status = 1;
		when 'update_subscription_plan' then
        
			set @plan_id = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.plan_id') );
			set @plan_name = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.plan_name') );
			set @amount = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.amount') );
            set @duration = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.duration') );
            -- select @plan_id, @plan_name,@amount,@duration;
            update subscription_plan set plan_name = @plan_name, amount= @amount, duration = @duration where hash_id = @plan_id and id > 0;
		when 'delete_plan' then
			set @plan_id = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.plan_id') );
            -- select @plan_id;
			update subscription_plan set status = -1 where hash_id = @plan_id and id > 0; 
    end case;
END