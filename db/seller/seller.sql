CREATE DEFINER=`admin`@`%` PROCEDURE `seller_procedure`(in task text(20), in input_data json)
BEGIN
	declare hashId text(255) default null;
    case task
		when 'create_seller' then
        set @name = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.name') );
		set @email = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.email') );
		set @phone = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.phone') );
        set @auth_id = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.authId') );
        insert into seller (
				name,
                email,
                phone,
                status,
                hash_id,
                auth_id
            ) values(
				@name,
                @email,
                @phone,
                0,
                createHashId("super_admin"),
                @auth_id
            );
		
        # Get seller list
        when 'seller_list' then
            select 
				sell.hash_id as shopId, 
                sell.name as shopName, 
                sell.email as email, 
                sell.phone as phone, 
                sell.status as shop_status, 
                sell.approvedBy as approved_by, 
                sell.product_delivary as delivary_option,
                sellSub.amount as subscription_amount,
                sellSub.subscription_date as subscription_date,
                sellSub.subscriptionCode as subscription_code,
                shopAdd.addressline as shop_address,
				shopAdd.city as shop_city,
				shopAdd.state as shop_state,
				shopAdd.country as shop_country,
				shopAdd.zip as shop_zip,
				shopAdd.location as shop_location,
                (select 
					json_array( 
						GROUP_CONCAT( 
							json_object( 
								'imageId', id,
                                'shop_image', shop_image, 
                                'shop_image_thumb', shop_image_thumb,
                                'shop_image_type', shop_image_type
							)  
						) 
					)  
				from shop_image where seller_id = sell.id) as shop_images
			from seller sell
            left join seller_subscription sellSub on sellSub.shop_id = sell.id
			left join shop_address shopAdd on shopAdd.seller_id = sell.id;
        
        # Get seller list
        when 'seller_list_city' then
			set @city = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.city') );
            select 
				sell.hash_id as shopId, 
                sell.name as shopName, 
                sell.email as email, 
                sell.phone as phone, 
                sell.status as shop_status, 
                sell.approvedBy as approved_by, 
                sell.product_delivary as delivary_option,
                sellSub.amount as subscription_amount,
                sellSub.subscription_date as subscription_date,
                sellSub.subscriptionCode as subscription_code,
                shopAdd.addressline as shop_address,
				shopAdd.city as shop_city,
				shopAdd.state as shop_state,
				shopAdd.country as shop_country,
				shopAdd.zip as shop_zip,
				shopAdd.location as shop_location,
                (select 
					json_array( 
						GROUP_CONCAT( 
							json_object( 
								'imageId', id,
                                'shop_image', shop_image, 
                                'shop_image_thumb', shop_image_thumb,
                                'shop_image_type', shop_image_type
							)  
						) 
					)  
				from shop_image where seller_id = sell.id) as shop_images
			from seller sell
            left join seller_subscription sellSub on sellSub.shop_id = sell.id
			left join shop_address shopAdd on shopAdd.seller_id = sell.id
			where shopAdd.city like @city and shopAdd.status = 1 ;
                
                
                
                
                
		# change seller status
        
        when 'change_seller_status' then
			set @sellerId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.sellerId') );
            set @status = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.status') );
            set @approvedBy = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.approvedBy') );
            
            UPDATE seller SET status = @status, approvedBy = @approvedBy where id>0 and hash_id = @sellerId;
		
        
        # get seller detils details by auth id
        
        when 'seller_details_by_auth' then
			set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.auth_id') );
            #set @shopId = (select id from seller where auth_id = hashId);
			select 
				sell.hash_id as shopId, 
                sell.name as shopName, 
                sell.email as email, 
                sell.phone as phone, 
                sell.status as shop_status, 
                sell.approvedBy as approved_by, 
                sell.product_delivary as delivary_option,
                sellSub.amount as subscription_amount,
                sellSub.subscription_date as subscription_date,
                sellSub.subscriptionCode as subscription_code,
                shopAdd.addressline as shop_address,
				shopAdd.city as shop_city,
				shopAdd.state as shop_state,
				shopAdd.country as shop_country,
				shopAdd.zip as shop_zip,
				shopAdd.location as shop_location,
                shopOwner.name  as owner,
                shopOwner.email as owner_email,
                shopOwner.phone as owner_phone,
                shopOwner.owner_address as owner_address,
                (select 
					json_array( 
						GROUP_CONCAT( 
							json_object( 
								'imageId', id,
                                'shop_image', shop_image, 
                                'shop_image_thumb', shop_image_thumb,
                                'shop_image_type', shop_image_type
							)  
						) 
					)  
				from shop_image where seller_id = sell.id) as shop_images
			from seller sell
            left join seller_subscription sellSub on sellSub.shop_id = sell.id
			left join shop_address shopAdd on shopAdd.seller_id = sell.id
            left join shop_owner shopOwner on shopOwner.shop_id = sell.id
            where sell.auth_id = hashId;
            
            
        # Shop Address Add
        when 'add_shop_address' then
			
			set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.seller_id') );
            set @seller_id = (select id from seller where hash_id = hashId);
			set @addressline = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.addressline') );
            set @city = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.city') );
            set @state = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.state') );
            set @country = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.country') );
            set @zip = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.zip') );
            set @location = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.location') );
           
            set @newHash = createHashId("shop_address");
            insert into shop_address (
				seller_id,
				addressline,
                city,
                state,
                country,
                zip,
                location,
                hash_id
            ) values (
				@seller_id,
				@addressline,
                @city,
                @state,
                @country,
                @zip,
                @location,
                @newHash
            );
            
        # update Shop Address Add
        when 'update_shop_address' then
			
			set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.seller_id') );
            set @seller_id = (select id from seller where hash_id = hashId);
			set @addressline = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.addressline') );
            set @city = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.city') );
            set @state = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.state') );
            set @country = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.country') );
            set @zip = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.zip') );
            set @location = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.location') );
           
            update shop_address set  
				addressline = @addressline,
                city = @city,
                state = @state,
                country = @country,
                zip = @zip,
                location = @location
            where seller_id = @seller_id and id > 1;
            
		# Shop image Add
        when 'add_shop_image' then
			
			set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.seller_id') );
            set @seller_id = (select id from seller where hash_id = hashId);
			set @shop_image = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.shop_image') );
            set @shop_image_thumb = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.shop_image_thumb') );
			set @shop_image_type = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.shop_image_type') );
            if ((select count(*) from shop_image where seller_id = @seller_id) < 1 ) 
				Then
				insert into shop_image (
					seller_id,
					shop_image,
					shop_image_thumb,
					shop_image_type,
					hash_id
				) values (
					@seller_id,
					@shop_image,
					@shop_image_thumb,
					@shop_image_type,
					createHashId("shop_image")
				);
			ELSE
				update shop_image set shop_image = @shop_image, shop_image_thumb = @shop_image_thumb where seller_id = @seller_id and id > 1;
            END IF;
            
		
        # Shop owner information information
        when 'add_shop_owner' then
			
			set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.shop_id') );
            set @shop_id = (select id from seller where hash_id = hashId);
			set @name = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.name') );
            set @email = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.email') );
			set @phone = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.phone') );
			set @owner_address = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.owner_address') );
            
            insert into shop_owner (
				shop_id,
                name,
                email,
                phone,
                owner_address,
                hash_id
            ) values (
				@shop_id,
                @name,
                @email,
                @phone,
                @owner_address,
                createHashId("shop_owner")
            );
            
		# Shop owner information information
        when 'update_shop_owner' then
			
			set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.shop_id') );
            set @shop_id = (select id from seller where hash_id = hashId);
			set @name = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.name') );
            set @email = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.email') );
			set @phone = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.phone') );
			set @owner_address = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.owner_address') );
            
            update shop_owner set
                name = @name,
                email = @email,
                phone = @phone,
                owner_address = @owner_address
			where 
				shop_id = @shop_id;
		
        # Shop woner address Add
        when 'add_shop_owner_address' then
			
			set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.shop_id') );
            set @shopId = (select id from seller where hash_id = hashId);
            set @shop_owner_id = (select id from shop_owner where shop_id = @shopId);
			set @addressline = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.addressline') );
            set @city = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.city') );
            set @state = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.state') );
            set @country = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.country') );
            set @zip = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.zip') );
           
            if ((select count(*) from shop_owner_address where shop_owner_id = @shop_owner_id) < 1 ) 
				Then
					insert into shop_owner_address (
						shop_owner_id,
						addressline,
						city,
						state,
						country,
						zip,
						hash_id
					) values (
						@shop_owner_id,
						@addressline,
						@city,
						@state,
						@country,
						@zip,
						createHashId("shop_owner_address")
					);
			ELSE
				select "shop owner address already exists" as message;
            END IF;
        
        # upload shop verificaton document
		when 'shop_verification' then
			set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.shop_id') );
            set @shop_id = (select id from seller where hash_id = hashId);
            #set @shop_owner_id = (select id from shop_owner where shop_id = @shopId);
			set @verification_document_no = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.verification_document_no') );
            set @verification_document_type = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.verification_document_type') );
            set @verification_document_image = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.verification_document_image') );
            set @verification_document_image_thumb = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.verification_document_image_thumb') );
            
            insert into shop_verification (
						shop_id,
                        shop_owner_id,
						verification_document_no,
						verification_document_type,
						verification_document_image,
						verification_document_image_thumb,
						hash_id
					) values (
						@shop_id,
						null,
						@verification_document_no,
						@verification_document_type,
						@verification_document_image,
						@verification_document_image_thumb,
						createHashId("shop_verification")
					);
		# upload shop owner verificaton document
		when 'shop_verification_owner' then
			set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.shop_id') );
            set @shop_id = (select id from seller where hash_id = hashId);
            set @shop_owner_id = (select id from shop_owner where shop_id = @shopId);
			set @verification_document_no = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.verification_document_no') );
            set @verification_document_type = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.verification_document_type') );
            set @verification_document_image = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.verification_document_image') );
            set @verification_document_image_thumb = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.verification_document_image_thumb') );
            
            insert into shop_verification (
						shop_id,
                        shop_owner_id,
						verification_document_no,
						verification_document_type,
						verification_document_image,
						verification_document_image_thumb,
						hash_id
					) values (
						null,
						@shop_owner_id,
						@verification_document_no,
						@verification_document_type,
						@verification_document_image,
						@verification_document_image_thumb,
						createHashId("shop_verification")
					);
                    
                    
        # add product on shop   
        when 'add_shop_product' then
        #seller
		set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.shop_id') );
		set @shop_id = (select id from seller where hash_id = hashId);
        
        #product color
        set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.product_color') );
		set @product_color_id = (select id from product_color where hash_id = hashId);
        
        #product
        set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.product_id') );
		set @product_id = (select id from product where hash_id = hashId);
        
        #product size
        set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.product_size') );
		set @product_size_id = (select id from product_size where hash_id = hashId);
        
        set @qty = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.qty') );
        
        set @mrp = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.mrp') );
        set @selling_price = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.selling_price') );
        set @discount = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.discount') );
        
        if ( (select count(*) from shop_product where shop_id = @shop_id and product_id = @product_id  and product_color_id = @product_color_id and product_size_id = @product_size_id ) < 1 ) then
			insert into shop_product(
				shop_id,
				product_id,
				product_size_id,
				product_color_id,
				qty,
				hash_id
            )values(
				@shop_id,
				@product_id,
				@product_size_id,
				@product_color_id,
				@qty,
				createHashId("shop_product")
            );
            set @last_shop_product =(select id from shop_product where shop_id = @shop_id and product_id = @product_id  and product_color_id = @product_color_id and product_size_id = @product_size_id);
            insert into shop_product_price(
				shop_product_id,
				mrp,
				selling_price,
				discount,
				hash_id
            )values(
				@last_shop_product,
				@mrp,
				@selling_price, 
				@discount,
				createHashId("shop_product_price")
            );
        else 
         select "Product already added" as message;
        end if; 
        
    # Get shop product 
    
    when 'all_shop_product' then
		set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.shop_id') );
        set @shop_id = (select id from seller where hash_id = hashId);
		select 
        prod.hash_id as master_product,
        prod.name as name,
        prod.model_no as model_no,
		prod.brand as brand,
        (select avg(rating) from ratings where product_id =  prod.id  and status =1) as avg_rating,
        (select count(*) from ratings where product_id =  prod.id and status =1 ) as total_rating,
		prod.description as description,
		prod.material as material,
		prod.catagory as catagory,
		prod.type as type,
		prod.brand as brand,
		prod.age_max as maxAge,
		prod.age_min as minAge,
		prod.occation as occation,
        #sProd.qty as qty,
        #sProdPrice.mrp as mrp,
        #sProdPrice.selling_price as selling_price,
        #sProdPrice.discount as discount,
		(select max(sp.selling_price) from shop_product_price sp, shop_product s where sp.shop_product_id = s.id and s.product_id = sProd.product_id ) as maxPrice,
		(select min(sp.selling_price) from shop_product_price sp, shop_product s where sp.shop_product_id = s.id and s.product_id = sProd.product_id ) as minPrice,   
        (select 
					json_array( 
						GROUP_CONCAT( 
							distinct json_object( 
								#'ID', id,
                                'productId', hash_id,
                                'mrp', (select mrp from shop_product_price where shop_product_id = sp.id ),
                                'selling', (select selling_price from shop_product_price where shop_product_id = sp.id ) 
							)  
						) 
					)  
		from shop_product sp where sp.product_id = prod.id ) as products,
        (select 
					json_array( 
						GROUP_CONCAT( 
							json_object( 
								'sizeId', (select hash_id from product_size where id = sProd1.product_size_id), 
                                'length', (select length from product_size where id = sProd1.product_size_id ), 
                                'size', (select size from product_size where id = sProd1.product_size_id ) 
							)  
						) 
					)  
                from shop_product sProd1 where sProd1.product_id = prod.id and sProd1.shop_id = shop.id ) as sizes,
        (select 
					json_array(
						group_concat(
							distinct json_object(
								'colorId',(select hash_id from product_color where id = sProd.product_color_id),
                                'color',(select color_image_thumb from all_product_color where id = (select color_id from product_color where id = sProd.product_color_id ) ), 
                                'colorName',(select name from all_product_color where id = (select color_id from product_color where id = sProd.product_color_id ) )
                                
							)
						)
					)  
		from shop_product sProd1 where sProd1.id = sProd.id ) as colors,
        (select 
				json_array(
					group_concat(
						json_object( 
							'image', product_image,
                            'thumb_image', product_image_thumb
                        )
                    )
                )
		from product_image prodImg where prodImg.product_color_id = sProd.product_color_id and prodImg.product_id = prod.id group by product_id) as images
        from shop_product sProd 
        inner join product prod on prod.id = sProd.product_id
        left join shop_product_price sProdPrice on sProd.id = sProdPrice.shop_product_id
        left join seller shop on sProd.shop_id = shop.id
        where sProd.shop_id =  @shop_id group by product_id; # group by product_id;
	# Get shop product with filter
    
    when 'all_shop_product_filter' then
		set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.shop_id') );
        set @shop_id = (select id from seller where hash_id = hashId);
        set @maxPrice = JSON_EXTRACT(input_data,'$.maxPrice');
        set @minPrice = JSON_EXTRACT(input_data,'$.minPrice');
        set @prodSize = JSON_EXTRACT(input_data,'$.productSizes');
        #select @prodSize;
		select 
        prod.hash_id as master_product,
        prod.name as name,
        prod.model_no as model_no,
		prod.brand as brand,
        #sProd.qty as qty,
        #sProdPrice.mrp as mrp,
        #sProdPrice.selling_price as selling_price,
        #sProdPrice.discount as discount,
		(select max(sp.selling_price) from shop_product_price sp, shop_product s where sp.shop_product_id = s.id and s.product_id = sProd.product_id ) as maxPrice,
		(select min(sp.selling_price) from shop_product_price sp, shop_product s where sp.shop_product_id = s.id and s.product_id = sProd.product_id ) as minPrice,   
        (select 
					json_array( 
						GROUP_CONCAT( 
							distinct json_object( 
								#'ID', id,
                                'productId', hash_id,
                                'mrp', (select mrp from shop_product_price where shop_product_id = sp.id ),
                                'selling', (select selling_price from shop_product_price where shop_product_id = sp.id ) 
							)  
						) 
					)  
		from shop_product sp where sp.product_id = prod.id ) as products,
        (select 
					json_array( 
						GROUP_CONCAT( 
							json_object( 
								'sizeId', (select hash_id from product_size where id = sProd1.product_size_id), 
                                'length', (select length from product_size where id = sProd1.product_size_id ), 
                                'size', (select size from product_size where id = sProd1.product_size_id ) 
							)  
						) 
					)  
                from shop_product sProd1 where sProd1.product_id = prod.id and sProd1.shop_id = shop.id ) as sizes,
        (select 
					json_array(
						group_concat(
							distinct json_object(
								'colorId',(select hash_id from product_color where id = sProd.product_color_id),
                                'color',(select color_image_thumb from all_product_color where id = (select color_id from product_color where id = sProd.product_color_id ) ), 
                                'colorName',(select name from all_product_color where id = (select color_id from product_color where id = sProd.product_color_id ) )
                                
							)
						)
					)  
		from shop_product sProd1 where sProd1.id = sProd.id ) as colors,
        (select 
				json_array(
					group_concat(
						json_object( 
							'image', product_image,
                            'thumb_image', product_image_thumb
                        )
                    )
                )
		from product_image prodImg where prodImg.product_color_id = sProd.product_color_id and prodImg.product_id = prod.id group by product_id) as images
        from shop_product sProd 
        inner join product prod on prod.id = sProd.product_id
        left join shop_product_price sProdPrice on sProd.id = sProdPrice.shop_product_id
        left join seller shop on sProd.shop_id = shop.id
        left join product_size prodSize on sProd.product_size_id = prodSize.id
        where 
        #prodSize.hash_id in (@prodSize) and 
        sProdPrice.mrp between @minPrice and @maxPrice and
        sProd.shop_id =  @shop_id group by product_id; # group by product_id;
    #get city list wher shoe store are available
    when 'shop_cities' then
		select distinct city from shop_address;
        
	# Other seller list who are selling the same product
	when 'other_seller_list' then
		set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.product_id') );
		set @product_id = (select id from product where hash_id = hashId);
        set @city =  JSON_UNQUOTE(JSON_EXTRACT(input_data,'$.city'));
        -- select @city;
        select 
        distinct
        sAddress.addressline as address,
        shop.name as shopName,
        shop.hash_id as shopId,
        sAddress.city as city,
        sAddress.location as shop_location,
		(select max(sp.selling_price) from shop_product_price sp, shop_product s where sp.shop_product_id = s.id and s.product_id = sProd.product_id ) as maxPrice,
		(select min(sp.selling_price) from shop_product_price sp, shop_product s where sp.shop_product_id = s.id and s.product_id = sProd.product_id ) as minPrice
        from shop_product sProd
        left join seller shop on sProd.shop_id = shop.id
        left join shop_address sAddress on sAddress.seller_id = shop.id
		where sProd.product_id = @product_id and sAddress.status = 1 and sAddress.city like @city;
        
        
	# get shop product details
    when 'shop_product_details' then
		set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.product_id') );
        set @product_id = (select id from shop_product where hash_id = hashId);
		select 
				sProd.hash_id as productId, 
				sProd.qty as qty,
				shop.name as shop,
                sAdd.addressline as shop_address,
				prod.name as name,
				prod.model_no as modelNo,
				prod.description as description,
				prod.material as material,
				prod.catagory as catagory,
				prod.type as type,
				prod.brand as brand,
				prod.age_max as maxAge,
				prod.age_min as minAge,
				prod.occation as occation,
				sProdPrice.mrp as mrp,
				sProdPrice.discount as discount,
				sProdPrice.selling_price as selling_price,
                (select avg(rating) from ratings where product_id =  prod.id and status =1 ) as avg_rating,
                (select count(*) from ratings where product_id =  prod.id and status =1 ) as total_rating,
				(select 
					json_array( 
						GROUP_CONCAT( 
							json_object( 
								'productId', hash_id,
                                'length', (select length from product_size where id = product_size_id ), 
                                'size', (select size from product_size where id = product_size_id ) 
							)  
						) 
					)  
				from shop_product where product_id = prod.id) as available_sizes,
				(select 
					json_array( 
						GROUP_CONCAT( 
							json_object( 
								'seller', name,
                                'shopId', hash_id 
                                -- 'address', (select addressline from shop_address where seller_id = s.id ) 
							)  
						) 
					)  
				from seller s where s.id = sProd.shop_id) as other_shop,
				(select 
						json_array(
							group_concat(
								distinct json_object(
									'productId', hash_id,
									'color',(select color_image_thumb from all_product_color where id = (select color_id from product_color where id = product_color_id ) ), 
									'colorName',(select name from all_product_color where id = (select color_id from product_color where id = product_color_id ) )
									
								)
							)
						)  
			from shop_product  where product_id = prod.id ) as available_colors,
			(select 
								json_array(
									group_concat(
										json_object(
											'image', product_image,
											'thumb_image', product_image_thumb
										)
									)
								)
			from product_image prodImg where prodImg.product_color_id = sProd.product_color_id and prodImg.product_id = prod.id group by product_id) as images
			
			from shop_product sProd
			left join product prod on sProd.product_id = prod.id
			left join shop_product_price sProdPrice on sProdPrice.shop_product_id = sProd.id 
            left join seller shop on sProd.shop_id = shop.id
            left join shop_address as sAdd on shop.id = sAdd.seller_id
            
			where sProd.id = @product_id and sProd.status = 1; 
    
    
    #get seller 
	# End of the case
	end case;
END