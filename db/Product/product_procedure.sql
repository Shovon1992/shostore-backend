CREATE DEFINER=`admin`@`%` PROCEDURE `product_procedure`(in task text(20), in input_data json )
BEGIN
	declare hashId varchar(50);
    case task
		when 'create_new_product' then
			
            set @name = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.name') );
            set @model_no = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.model_no') );
            set @description = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.description') );
            set @material = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.material') );
            set @catagory = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.catagory') );
            set @type = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.type') );
            set @brand = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.brand') );
            set @age_max = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.age_max') );
            set @age_min = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.age_min') );
            set @occation = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.occation') );
            set @create_by = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.create_by') );
            set @status = 1;
			set @hash_id = createHashId("product");
            #insert product details
			insert into product(
				name,
                model_no,
                description,
                material,
                catagory,
                type,
                brand,
                age_max,
                age_min,
                occation,
                hash_id
            ) values(
				@name,
                @model_no,
                @description,
                @material,
                @catagory,
                @type,
                @brand,
                @age_max,
                @age_min,
                @occation,
                @hash_id
            );
            
            # insert data in product size table
            
           # select * from product where hash_id= @hash_id;
            
		when 'add_product_size' then
			set @length = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.length') );
			set @size = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.size') );
            
            # For product id
			set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.producti_id') );
            set @producti_id = (select id from product where hash_id = hashId);
            
            insert into product_size (
				producti_id,
                length,
                size,
                hash_id
            ) values (
				@producti_id,
				@length,
                @size,
                createHashId("product_size")
            );
            
		#get product images
        
        
        when 'add_product_color' then   
        
			# For product id
			set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.producti_id') );
            set @producti_id = (select id from product where hash_id = hashId);
            
            # For color id
            set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.color_id') );
            set @color_id = (select id from all_product_color where hash_id = hashId);
            
            insert into product_color (
				producti_id,
                color_id,
                hash_id
            ) values (
				@producti_id,
                @color_id,
                createHashId("product_size")
            );
            
        when 'add_product_images' then
			# For product id
			set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.product_color_id') );
            set @product_color_id = (select id from product_color where hash_id = hashId);
            set @product_id = (select distinct producti_id from product_color where hash_id = hashId);
            # For product id
			set @product_image = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.product_image') );
            set @product_image_thumb = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.product_image_thumb') );
            
            insert into product_image (
				product_id,
				product_color_id,
                product_image,
                product_image_thumb,
                hash_id
            ) values(
				@product_id,
				@product_color_id,
                @product_image,
                @product_image_thumb,
                createHashId("product_image")
            );
            
		when 'get_all_product' then
			select 
				prod.hash_id as productId,
				prod.name as product,
                prod.model_no as model,
                prod.description as details,
                prod.brand as brand,
                prod.material as material,
                prod.catagory as catagory,
                prod.type as type,
                prod.occation as occation,
				(select 
					json_array( 
						GROUP_CONCAT( 
							json_object( 
								'sizeId',hash_id,'length', 
                                length, 'size', size 
							)  
						) 
					)  
                from product_size prodSize where prodSize.producti_id = prod.id group by producti_id) as sizes,
                
                # all colors of a product
				(select 
					json_array(
						group_concat(
							json_object(
								'colorId',hash_id,
                                'color',(select color_image_thumb from all_product_color where id = color_id ), 
                                'colorName',(select name from all_product_color where id = color_id )
                                
							)
						)
					)  
			from product_color prodColor where prodColor.producti_id = prod.id group by producti_id) as colors,
            #All images associated with product
            (select 
				json_array(
					group_concat(
						json_object(
							'image', product_image,
                            'thumb_image', product_image_thumb
                        )
                    )
                )
            from product_image prodImg where prodImg.product_id = prod.id group by product_id) as images
			from product prod; 
            
	when 'get_all_product_not_in_shop' then
    set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.shop_id') );
    set @shop_id = ( select distinct id from seller where hash_id = hashId);
			select 
				prod.hash_id as productId,
				prod.name as product,
                prod.model_no as model,
                prod.description as details,
                prod.brand as brand,
                prod.material as material,
                prod.catagory as catagory,
                prod.type as type,
                prod.occation as occation,
                (select avg(rating) from ratings where product_id =  prod.id  and status =1) as avg_rating,
				(select count(*) from ratings where product_id =  prod.id and status =1 ) as total_rating,
                (select count(*) from ratings where product_id =  prod.id and comment !='' and status =1 ) as total_reviews,
				(select 
					json_array( 
						GROUP_CONCAT( 
							json_object( 
								'sizeId',hash_id,'length', 
                                length, 'size', size 
							)  
						) 
					)  
                from product_size prodSize where prodSize.producti_id = prod.id group by producti_id) as sizes,
                
                # all colors of a product
				(select 
					json_array(
						group_concat(
							json_object(
								'colorId',hash_id,
                                'color',(select color_image_thumb from all_product_color where id = color_id ), 
                                'colorName',(select name from all_product_color where id = color_id )
                                
							)
						)
					)  
			from product_color prodColor where prodColor.producti_id = prod.id group by producti_id) as colors,
            #All images associated with product
            (select 
				json_array(
					group_concat(
						json_object(
							'image', product_image,
                            'thumb_image', product_image_thumb
                        )
                    )
                )
            from product_image prodImg where prodImg.product_id = prod.id group by product_id) as images
			from product prod where
            prod.id not in (select distinct id from shop_product where shop_id = ( select id from seller where hash_id = hashId ));
           
            
       when 'add_shop_product' then 
			
            # product color
			set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.product_id') );
            set @product_id = (select id from product where hash_id = hashId);
            
            # product color
			set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.product_color') );
            set @product_color_id = (select id from product_color where hash_id = hashId);
            
            # product color
			set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.product_size') );
            set @product_size_id = (select id from product_size where hash_id = hashId);
            
            # product color
			set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.shop_id') );
            set @shop_id = (select id from seller where hash_id = hashId);
       
       when 'add_product_rating' then
			
			# product Id
			set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.product_id') );
            set @product_id = (select id from product where hash_id = hashId);
            
            # user Id
			set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.user_id') );
            set @user_id = (select id from user where hash_id = hashId);
            
            # shop Id
			set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.seller_id') );
            set @shop_id = (select id from seller where hash_id = hashId);
            
            set @rating = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.rating') );
			set @comment = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.comment') );
			set @rating_for = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.rating_for') ); # 1 for product , 2 for seller
            
            insert into ratings(
				user_id,
                rating,
                comment,
                rating_for,
                shop_id,
                product_id,
                create_by,
                hash_id
            )values(
				@user_id,
                @rating,
                @comment,
                @rating_for,
                @shop_id,
                @product_id,
                create_by,
                @user_id,
                createHashId("ratings")
            );
            
	when 'get_product_rating' then
		set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.id') );
		set @product_id = (select id from product where hash_id = hashId);
        
		select 
			avg(rating) as avg_rating,
            count(*) as total_ratings,
            (select json_array(
				group_concat(
					json_object(
						'rating', rating,
                        'comment', comment,
                        'rate_by', (select name from user where id = user_id),
                        'rated_on', created_on
                    )
                )
            )from ratings group by product_id) as all_ratings
        from ratings rat
        where product_id = @product_id and rating_for = 1 ;
        
	when 'get_seller_rating' then
		set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.id') );
		set @shop_id = (select id from product where hash_id = hashId);
        
		select 
			avg(rating) as avg_rating,
            count(*) as total_ratings,
            (select json_array(
				group_concat(
					json_object(
						'rating', rating,
                        'comment', comment,
                        'rate_by', (select name from user where id = user_id),
                        'rated_on', created_on
                    )
                )
            )from ratings group by product_id) as all_ratings
        from ratings
        where shop_id = @shop_id and rating_for = 2 ;
        
        when 'product_details_by_color_size' then
        
        #product color
        set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.master_product') );
		set @product_id = (select id from product where hash_id = hashId);
		
        #product color
        set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.product_color') );
		set @product_color_id = (select id from product_color where hash_id = hashId);
        
        
        #product size
        set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.product_size') );
		set @product_size_id = (select id from product_size where hash_id = hashId);
        
		#seller
            set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.shop_id') );
            set @shop_id = (select id from seller where hash_id = hashId);
        
        
		select 
				distinct 
                JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.product_color') ) as selectedColor,
                JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.product_size') ) as selectedSize,
				sProd.hash_id as productId, 
				sProd.qty as qty,
				shop.name as shop,
                sAdd.addressline as shop_address,
				prod.name as name,
				prod.model_no as modelNo,
				(select avg(rating) from ratings where product_id =  prod.id  and status =1) as avg_rating,
				(select count(*) from ratings where product_id =  prod.id and status =1 ) as total_rating,
                (select count(*) from ratings where product_id =  prod.id and comment !='' and status =1 ) as total_reviews,
				prod.description as description,
				prod.material as material,
				prod.catagory as catagory,
				prod.type as type,
				prod.brand as brand,
				prod.age_max as maxAge,
				prod.age_min as minAge,
				prod.occation as occation,
				sProdPrice.mrp as mrp,
				sProdPrice.discount as discount,
				sProdPrice.selling_price as selling_price,
				(select 
					json_array(
						group_concat(
							json_object(
								'sizeId', ps.hash_id,
								'length', ps.length,
                                'size' , ps.size
							)
						)
					)
				from product_size ps where ps.producti_id = prod.id  and sProd.shop_id = shop.id and ps.size > 0 and ps.length > 0 and ps.status = 1) as all_sizes,
                (select 
					json_array(
						group_concat(
							json_object(
                                'size' , (select size from product_size where id = sp.product_size_id),
                                 'length' , (select length from product_size where id = sp.product_size_id),
                                  'sizeId' , (select hash_id from product_size where id = sp.product_size_id)
							)
						)
					)
				from shop_product sp where sp.shop_id = @shop_id and sp.product_id = @product_id ) as available_sizes,
				(select 
					distinct json_array(
						group_concat(
							json_object(
								'colorId', hash_id,
                                'color' , ( select name from all_product_color where id = pc.color_id ),
								'color_image' ,( select color_image from all_product_color where id = pc.color_id ),
                                'color_image_thumb' ,( select color_image_thumb from all_product_color where id = pc.color_id )
							)
						)
					)
				from product_color pc where pc.producti_id = prod.id and pc.status = 1) as all_colors,
				(select 
					distinct json_array(
						group_concat(
							json_object(
								'colorId', ( select hash_id from product_color where id = sp.product_color_id),
                                'color' , ( select name from all_product_color where id = ( select color_id from product_color where id = sp.product_color_id) ),
								'color_image' ,( select color_image from all_product_color where id = ( select color_id from product_color where id = sp.product_color_id) ),
                                'color_image_thumb' ,( select color_image_thumb from all_product_color where id = ( select color_id from product_color where id = sp.product_color_id) )
							)
						)
					)
				from shop_product sp where sp.shop_id = @shop_id and sp.product_id = @product_id and sp.status = 1 group by sp.id) as available_colors,
			(select 
								json_array(
									group_concat(
										json_object(
											'image', product_image,
											'thumb_image', product_image_thumb
										)
									)
								)
			from product_image prodImg where prodImg.product_color_id = sProd.product_color_id and prodImg.product_id = prod.id group by product_id) as images
			
			from shop_product sProd
			inner join product prod on sProd.product_id = prod.id
			inner join shop_product_price sProdPrice on sProdPrice.shop_product_id = sProd.id 
            inner join seller shop on sProd.shop_id = shop.id
            inner join shop_address as sAdd on shop.id = sAdd.seller_id
			where sProd.product_id = @product_id and sProd.shop_id = @shop_id and sProd.product_size_id = @product_size_id and sProd.product_color_id = @product_color_id
            and sAdd.status =1 ; 
            
	when 'color_size_list_shop_product' then 
    set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.shop_id') );
    set @shop_id = ( select id from seller where hash_id = hashId);
    set hashId = JSON_UNQUOTE( JSON_EXTRACT(input_data,'$.master_product_id') );
	set @master_product_id = ( select id from product where hash_id = hashId);
		select 
			sPrice.mrp,
			sPrice.selling_price,
			sPrice.discount,
			sp.qty,
			(select size from product_size where id = sp.product_size_id ) as product_size,
			(select color_image_thumb from all_product_color where id = (select color_id from product_color where id = sp.product_color_id )) as color_image,
			(select name from all_product_color where id = (select color_id from product_color where id = sp.product_color_id )) as color_name
		from shop_product sp
		inner join shop_product_price sPrice on sPrice.shop_product_id = sp.id
		where sp.shop_id = @shop_id and sp.product_id = @master_product_id and sp.status = 1;

	end case;
END